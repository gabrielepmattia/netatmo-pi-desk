# Netatmo Pi Desk

![[preview](https://i.imgur.com/poKAlCx.jpg)](https://i.imgur.com/poKAlCxl.jpg)

This repository is for setting up a netatmo dashboard within the RPi as a kiosk.

1. install Docker and Docker Compose
2. clone this repository inside the RPi
3. start all the services with `docker-compose up -d`
4. go to `http://<rpi-ip>:8080/auth`
5. proceed with the authorization and when returning you need replace in the address bar 0.0.0.0 with the IP of your Raspberry Pi
6. open grafana `http://<rpi-ip>:3000/`, login and create a new organization called `Public`
7. in that organization import the dashboard `grafana-dash.json`

## Kiosk Mode & Autostart

After setting the Pi to start in GUI mode automatically, install the package `unclutter`

```
sudo apt install unclutter
```

Then create the file `~/.config/lxsession/LXDE-pi/autostart` with the content:

```bash
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash point-rpi

# auto start dashboard
@chromium-browser --start-fullscreen --start-maximized http://localhost:3000/d/hM5OtUkRk/desk?orgId=2&refresh=1m&kiosk

# disable cursor
@unclutter -idle 0

# disable screensaver
@xset s noblank 
@xset s off 
@xset -dpms
```
This file has been built from [[1](https://brianhaines.com/2019/01/08/auto-open-website-on-raspberry-pi-startup/)], [[2](https://2021.jackbarber.co.uk/blog/2017-02-16-hide-raspberry-pi-mouse-cursor-in-raspbian-kiosk)] and [[3](https://www.etcwiki.org/wiki/Disable_screensaver_and_screen_blanking_Raspberry_Pi)].

# Dashboards

## Basic

The default dashboard is the following:

![[dashboard-basic](https://i.imgur.com/4UEKUcw.png)](https://i.imgur.com/4UEKUcwl.png)

## Time-Sun-Moon

For this dashboard you need to install the [Clock](https://grafana.com/grafana/plugins/grafana-clock-panel/) and the [Sun and Moon](https://grafana.com/grafana/plugins/fetzerch-sunandmoon-datasource/) plugins.

![[dashboard-time-sun-moon](https://i.imgur.com/KE2tKQy.png)](https://i.imgur.com/KE2tKQyl.png)